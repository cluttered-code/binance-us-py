from setuptools import setup

setup(
    name='binance-us',
    version='0.0.0',
    packages=['binance_us'],
    url='https://gitlab.com/cluttered-code/binance-us-py',
    license='ISC',
    author='David Clutter',
    author_email='cluttered.code@gmail.com',
    description='Binance US Client - Python',
    install_requires=[
        'httpx',
        'websockets'
    ]
)
