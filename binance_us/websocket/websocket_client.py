import asyncio
import json
from abc import ABCMeta, abstractmethod
from typing import Union

import websockets
from websockets import ConnectionClosedOK, WebSocketClientProtocol

import logging


class WebSocketClient(metaclass=ABCMeta):
    _uri: str
    _ping_interval: float
    _connection: WebSocketClientProtocol
    _done: bool = False

    def __init__(self, uri: str, ping_interval: float = 30):
        self._uri = uri
        self._ping_interval = ping_interval

    @abstractmethod
    async def message(self, ws: WebSocketClientProtocol, message: Union[str, bytes]):
        return NotImplemented

    async def start(self):
        while not self._done:
            logging.info(f"connecting websocket '{self._uri}'...")
            self._connection = websockets.connect(self._uri, ping_interval=self._ping_interval)
            try:
                async with self._connection as ws:  # type: WebSocketClientProtocol
                    logging.info(f"'{self._uri}' websocket connected")
                    while not self._done:

                        message = await ws.recv()
                        # pylint: disable=no-value-for-parameter
                        asyncio.create_task(self.message(ws, message))

            except ConnectionClosedOK:
                pass  # finish
            except:
                await asyncio.sleep(1)
                pass  # connect again

    async def close(self):
        logging.info(f"closing '{self._uri}' websocket...")
        self._done = True
        await self._connection.ws_client.close(reason="shutdown")