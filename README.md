# Binance US Client - Python

## Virtual Environment

`.venv` will automatically be used in vscode terminal.

### Initialize
```bash
# Windows
python -m venv .venv

# Linux
python3.8 -m venv .venv
```

### Activate
```bash
source .venv/bin/activate
```

### Dependencies
```bash
# Windows
pip install -U pip setuptools wheel
pip install -r requirements/test.txt

# Linux
pip install -U pip setuptools wheel
pip install -r requirements/test.linux.txt
```

## Run

### Docker
```bash
docker-compose up -d --build --force-recreate
```

## Test
Eared dove requires python 3.8 to run tests locally

```bash
# TDD
ptw

# Single
pytest --cov=eared_dove --cov-report=term --cov-report=html
```